#!/usr/bin/env python
from imports import *
import cgitb
cgitb.enable()
if __name__ == "__main__" :
    form = cgi.FieldStorage()
    if "user" not in form :
        t = env.get_template("register.html")
        print("Content-type: text/html\n")
        print(t.render())
    else :
        handler = cookiecheck()
        passwd = form["passwd"].value
        username = form["user"].value
        with Session(engine) as session:
            if len(session.execute(select(User).where(User.username == username)).scalars().all()) != 0 :
                print("Content-type: text/html\n")
                print(render_error("Registration Systems", "That username has already been taken"))
            else :
                poss_chars = list(string.printable)
                salt = ""
                login_uuid = str(uuid.uuid4())
                for x in range(6) :
                    salt += random.choice(poss_chars)
                this_user = User(username=username, password_hash=hashlib.sha512((passwd + salt).encode("utf-8")).hexdigest(), password_salt=salt, login_uuid=login_uuid)
                session.add(this_user)
                session.commit()
                c = cookies.SimpleCookie()
                c["user"] = username
                c["uuid"] = login_uuid
                print(c)
                print("Location: /\n")
                #print("Content-type: text/plain\n")
                #print("Content-type: text/plain\n")
                #print("Registered successfully")
    
