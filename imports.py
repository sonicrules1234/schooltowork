import cgi, string, random, hashlib, os, uuid, cgitb
#cgitb.enable()
from http import cookies
from database import *
from jinja2 import Environment, FileSystemLoader, select_autoescape
env = Environment(
    loader=FileSystemLoader("templates/"),
    autoescape=select_autoescape()
)
def cookiecheck() :
    if 'HTTP_COOKIE' in os.environ :
        c = os.environ['HTTP_COOKIE']
        #c = c.split('; ')
        a = cookies.BaseCookie()
        a.load(c)
        
        handler = {}
        for k, v in a.items() :
            #cookie = cookie.split("=")
            handler[k] = v.value
        return handler
    else : return {}
def is_authed() :
    handler = cookiecheck()
    if "user" in handler and "uuid" in handler :
        with Session(engine) as session:
            record = session.execute(select(User).where(User.username == handler["user"])).fetchone()[0]
            if record.login_uuid == handler["uuid"] :
                return handler["user"]
    return ""
def render_error(system, error) :
    error_template = env.get_template("error.html")
    return error_template.render(error_message=f"{system} has encountered an error: {error}")