#!/usr/bin/env python
from imports import *
from database import *
template = env.get_template("index.html")
l_template = env.get_template("login.html")
print("Content-type: text/html\r\n")
if is_authed() :
    print(template.render())
else :
    print(l_template.render())