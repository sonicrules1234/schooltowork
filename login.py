#!/usr/bin/env python
from imports import *
import uuid
if __name__ == "__main__" :
    authed_as = is_authed()
    if not authed_as :
        form = cgi.FieldStorage()
        handler = cookiecheck()
        if "user" in form and 'passwd' in form :
            passwd = form["passwd"].value
            username = form["user"].value
            with Session(engine) as session:
                user = session.execute(select(User).where(User.username == username)).fetchone()[0]
                if user.password_hash == hashlib.sha512((passwd + user.password_salt).encode("utf-8")).hexdigest() :
                    c = cookies.SimpleCookie()
                    c["user"] = username
                    c["uuid"] = str(uuid.uuid4())
                    user.login_uuid = c["uuid"].value
                    session.commit()
                    print(c)
                    print("Location: /\n")
                else :
                    print("Content-type: text/html\n")
                    print(render_error("Login Systems", "That username and/or password is incorrect"))
        else :
            t = env.get_template("login.html")
            print("Content-type: text/html\n")
            print(t.render())
    else :
        form = cgi.FieldStorage()
        if "logout" in form :
            c = cookies.SimpleCookie()
            c["username"] = ""
            c["uuid"] = ""
            with Session(engine) as session:
                user = session.execute(select(User).where(User.username == authed_as)).fetchone()[0]
                user.login_uuid =  str(uuid.uuid4())
                session.commit()
            print(c)
        print("Location: /\n")
